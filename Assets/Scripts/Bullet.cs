using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float bulletSpeed;
    [SerializeField] private int bulletDamage;


    public int _bulletDamage { get { return bulletDamage; } private set { bulletDamage = value; } }


    private void Update()
    {
        moveBullet();
        StartCoroutine(DestroyObj());
    }

    private void moveBullet()
    {
    
        transform.Translate( -transform.right * bulletSpeed * Time.deltaTime);
    }

    private IEnumerator DestroyObj()
    {
        yield return new WaitForSeconds(3f);
        ProjectilePool.Instance.ReturnToPool(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            IDamageable damageable = collision.collider.GetComponent<IDamageable>();
            damageable?.TakeDamage(bulletDamage);

            ProjectilePool.Instance.ReturnToPool(gameObject);


        }
    }

}
