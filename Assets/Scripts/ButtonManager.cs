using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private PlayerController playerController;
    [SerializeField] private List<GameObject> myGO = new List<GameObject>();

    private CheckPointsManager CM;
    private void Start()
    {
        CM = CheckPointsManager.Instance;
    }
    public void ChangeScene()
    {
        playerController.transform.position = CM._startSave;
        playerController._health = playerController._maxHealth;
        CM._currentSave = CM._startSave;

        GameManager.instance._score = 0;

        foreach (GameObject go in myGO)
        {
            go.SetActive(true);
        }
    }
    public void GameContinue()
    {
        playerController.transform.position = CM._currentSave;
        playerController._health = playerController._maxHealth;
    }
    
    public void RestartScene()
    {
        SceneManager.LoadScene(0);
    }
    

}
