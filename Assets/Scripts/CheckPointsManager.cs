using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointsManager : MonoBehaviour
{

    [SerializeField] private PlayerController playerController; 
    [SerializeField] private Vector2 currentSave;
    [SerializeField] private Vector2 startSave;

    public static Action<Transform> OnSave;

    private static CheckPointsManager instance;
    public static CheckPointsManager Instance
    {
        get => instance;
        private set => instance = value;
    }


    public Vector2 _currentSave { get => currentSave; set => currentSave = value; }
    public Vector2 _startSave {  get => startSave;} 


    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(this);

        startSave = playerController.transform.position;
        currentSave = startSave;

    }

    private void OnEnable()
    {
        OnSave += SavePosition;
    }

    private void OnDisable()
    {
        OnSave -= SavePosition;
    }

    private void SavePosition(Transform obj)
    {
        currentSave = obj.position;
    }

}
