using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            StartCoroutine(dalay(collision));
    }
    IEnumerator dalay(Collider2D collider)
    {
        yield return new WaitForSeconds(0.5f);
        IDamageable damageable = collider.GetComponent<IDamageable>();
        damageable?.TakeDamage(100);

    }
}
