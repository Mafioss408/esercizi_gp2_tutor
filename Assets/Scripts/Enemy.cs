using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Enemy : MonoBehaviour,IDamageable
{
    private SpawnerPool pool;
    private PlayerController controller;
    private bool canShoot = true;


    [SerializeField] private float health;
    [SerializeField] private float maxHealth;
    [SerializeField] private float speed;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private Transform muzzle;



    private void Awake()
    {
        pool = GetComponent<SpawnerPool>();
        controller = FindObjectOfType<PlayerController>();
    }
    private void Update()
    {
        Shoot();
    }
    private void OnEnable()
    {
        health = maxHealth;
    }

    private void Shoot()
    {
        if (Physics2D.OverlapCircle(transform.position, 10, playerMask))
        {
            if(canShoot)
            {
                pool.Spawn();
                StartCoroutine(dalay());
            }
        }
    }

    private IEnumerator dalay()
    {
        canShoot = false;
        yield return new WaitForSeconds(1);
        canShoot = true;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        if(health <= 0 ) 
        {
            GameManager.instance._score += 5;
            gameObject.SetActive(false);
        }
    }
}
