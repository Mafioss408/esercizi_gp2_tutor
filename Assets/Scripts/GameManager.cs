using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager Instance;
    public static GameManager instance
    {
        get => Instance;
        set => Instance = value;
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }

    [SerializeField] private int score;


    private bool win = false;
    private bool loose = false;

    #region get/set
    public int _score { get => score; set => score = value; }
    public bool _loose { get => loose; set => loose = value; }
    public bool _win { get => win; set => win = value; }
    #endregion

}
