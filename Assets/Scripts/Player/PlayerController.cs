using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour, IDamageable
{

    private Rigidbody2D rb;
    private Vector2 touchStartPosition, touchEndPosition;

    public bool groundCheck = false;

    private DirectionType directionType = DirectionType.Still;
    private PlayerAction action = PlayerAction.move;


    [Header("Variable Move\n")]
    [SerializeField] private float health;
    [SerializeField] private float  maxHealth;
    [SerializeField] private float speed;
    [SerializeField] private float maxVelocity;
    [SerializeField] private float dpadSens = 0.2f;
    [Header("Reference\n")]
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Transform ground;
    [SerializeField] private Image hpBar;
    [SerializeField] public Vector2 playerPosition;
    [SerializeField] private GameObject looseMenu;


    public float _health { get =>  health; set => health = value; }
    public float _maxHealth { get => maxHealth; set => maxHealth = value; }
    public Rigidbody2D _rb { get => rb; set => rb = value; }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        health = maxHealth;
        directionType = DirectionType.Still;
    }

    private void Update()
    {
        playerPosition = transform.position;
        RefreshUI();
    }


    void FixedUpdate()
    {
        Move();
        OnGround();
    }

    private void Move()
    {
        if (Input.touchCount <= 0)
        {
            return;
        }

        Touch touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Began)
        {
            touchStartPosition = touch.position;
        }

        else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Ended)
        {
            touchEndPosition = touch.position;

            float x = touchEndPosition.x - touchStartPosition.x;

            if (Mathf.Abs(x) <= dpadSens)
            {
                directionType = DirectionType.Still;
            }
            else if (Mathf.Abs(x) > dpadSens)
            {
                directionType = x > 0 ? DirectionType.Right : DirectionType.Left;
            }
        }

        if (action == PlayerAction.move)
            MoveCube();
    }

    private void MoveCube()
    {
        Vector2 moveDir = Vector2.zero;

        switch (directionType)
        {
            case DirectionType.Still:
                moveDir = Vector2.zero;
                break;
            case DirectionType.Right:
                moveDir = Vector2.right;
                break;
            case DirectionType.Left:
                moveDir = Vector2.left;
                break;
            default:
                break;
        }
        rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -maxVelocity, maxVelocity), rb.velocity.y);
        rb.AddForce(moveDir * speed * Time.deltaTime, ForceMode2D.Impulse);
    }
    private void OnGround()
    {
        if (Physics2D.OverlapCircle(ground.transform.position, 0.2f, groundMask))
            groundCheck = true;
        else
            groundCheck = false;
    }

  
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(ground.transform.position, 0.2f);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 10f);
    }

#endif
    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
           looseMenu.SetActive(true);
           UIManager.onUI?.Invoke();
        }
            
    }

    private void RefreshUI()
    {
        hpBar.fillAmount = health / maxHealth;
    }
}
