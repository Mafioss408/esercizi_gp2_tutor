using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : MonoBehaviour
{
    private PlayerAction action;
    private Vector3 touchPosition;
    private bool canDash = false;

    [Header("Reference\n")]
    [SerializeField] private LayerMask enemyMask;


    private void Update()
    {
        Attack();
        Dash(touchPosition);
    }

    private void Attack()
    {
        if (Input.touchCount <= 0) return;

        if (Physics2D.OverlapCircle(transform.position, 10, enemyMask))
        {
            Touch touch = Input.GetTouch(0);

            touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
            touchPosition.z = 0;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, touchPosition - transform.position, Mathf.Infinity, enemyMask);


            if (hit.collider == null) return;
            if (hit.collider.gameObject.layer == 6) return;
            if (hit.collider.gameObject.layer == 7)
            {
                IDamageable damageable = hit.collider.gameObject.GetComponent<IDamageable>();

                action = PlayerAction.dash;
                canDash = true;
            }
        }
        else
            return;
    }

    private void Dash(Vector3 position)
    {
        if (canDash)
        {
            Physics.IgnoreLayerCollision(3, 7);
            Physics.IgnoreLayerCollision(3, 6);
            transform.position = Vector3.MoveTowards(transform.position, position, 50 * Time.deltaTime);

            if (Vector3.Distance(transform.position, position) <= 0.2f)
            {
                action = PlayerAction.move;
                canDash = false;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (canDash)
        {
            IDamageable damageable = collision.collider.GetComponent<IDamageable>();
            damageable?.TakeDamage(100);
       
        }    
    }
}
