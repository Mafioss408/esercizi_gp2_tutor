using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    private PlayerController controller;
    private PlayerAction action;

    private bool canJump = false;

    [Header("Variable Jump\n")]
    [SerializeField] private float jumpHeight;
    [SerializeField] private float jumpHeightSecond;


    private void Awake()
    {
        controller = GetComponent<PlayerController>();
    }
    public void Jump()
    {
        if (controller.groundCheck)
        {
            action = PlayerAction.move;
            JumpSetting(jumpHeight);
            canJump = true;
        }
        else if (canJump)
        {
            action = PlayerAction.jump;
            JumpSetting(jumpHeightSecond);
            canJump = false;
        }
    }

    public void ChangeState()
    {
        action = PlayerAction.jump;
    }

    private void JumpSetting(float jump)
    {
        float jumpForce = Mathf.Sqrt(jump * -2 * (Physics2D.gravity.y * controller._rb.gravityScale));
        controller._rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
    }

}
