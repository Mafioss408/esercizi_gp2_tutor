using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPool : MonoBehaviour
{
    private ProjectilePool pool;

    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform muzzle;

    private void Start()
    {
        pool = ProjectilePool.Instance;
    }
    public void Spawn()
    {
        GameObject bomb = pool.Get(bullet);
        bomb.transform.position = muzzle.transform.position;
        bomb.transform.right = muzzle.transform.forward;
        bomb.gameObject.SetActive(true);
    }
}
