public enum DirectionType
{
    Still,
    Right,
    Left
}

public enum PlayerAction
{
    noAction,
    move,
    jump,
    dash
}
