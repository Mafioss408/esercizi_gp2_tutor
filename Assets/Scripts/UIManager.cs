using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public delegate void OnUI();
    public static OnUI onUI;

    [SerializeField] private TextMeshProUGUI scoreUI;
    [SerializeField] private TextMeshProUGUI winUI;
    [SerializeField] private TextMeshProUGUI looseUI;

    private void Update()
    {
        scoreUI.text = "Score: " + GameManager.instance._score;
    }

    private void UIRefresh()
    {
        winUI.text = scoreUI.text;
        looseUI.text = scoreUI.text;
    }
    private void OnEnable()
    {
        onUI += UIRefresh;
    }
    private void OnDisable()
    {
        onUI -= UIRefresh;
    }

}
