using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    [SerializeField] private GameObject winCanvas;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StartCoroutine(win());
        }
    }
        
    private IEnumerator win()
    {
        yield return new WaitForSeconds(1);
        winCanvas.SetActive(true);
        UIManager.onUI?.Invoke();
    }
}
